# Supplementary material for enumeration of the families of directed (multi-)graphs

This repository contains `Python` code and `IPython` notebooks for a complete verification of the corresponding paper "The birth of the strong components" by *Élie de Panafieu, Sergey Dovgal, Dimbinaina Ralaivaosaona, Vonjy Rasendrahasina, and Stephan Wagner* available online as an arxiv preprint <https://arxiv.org/abs/2009.12127>.

In the paper, we provide several known generating functions like the generating functions of DAGs, along with some relatively new ones, which is the generating function of *elementary* digraphs (whose strongly connected component all have excess at most zero), and digraphs with given strongly connected components (for example with exactly one bicyclic component).

We apply asymptotic tools to extract the limiting probabilities for these digraph families with a particular focus on the region where the phase transition happens (i.e. where the *giant component* appears).

The easiest way to access the contents of this repository is through looking at the IPython Notebooks presenting the material in the most friendly fashion.

## IPython Notebooks

The directory `ipynb` contains `IPython` notebooks which are detailed computation reports for each section of the paper that contains computation. The notebooks are executed using `Python 3` and may require installing additional python libraries.

In addition, it is possible to view the contents of these notebooks without installing any additional software using `nbviewer` (links provided separately to each notebook below).

### Section 1. Introduction

* [`Section 1.ipynb`][Sec 1]. Numerical evaluation of the constants arising from complex contour integrals, with high precision.

### Section 3. Symbolic method
* [`Section 3.ipynb`][Sec 3]. Validation of the series expansion using an exhaustive enumeration.

### Section 4. Inner integrals
* [`Section 4.2.ipynb`][Sec 4.2]. Airy integrals and Taylor series expansions.
* [`Section 4.3.ipynb`][Sec 4.3]. Roots and derivatives of the deformed exponent.

### Section 5. External integrals
* [`Section 5.1.ipynb`][Sec 5.1]. The case of multidigraphs
* [`Section 5.2.ipynb`][Sec 5.2]. The case of simple digraphs

### Section 6. Multidigraph families
* [`Section 6.1.ipynb`][Sec 6.1]. Asymptotics of directed acyclic graphs
* [`Section 6.2.ipynb`][Sec 6.2]. Asymptotics of elementary digraphs and complex components
* [`Section 6.x.ipynb`][Sec 6.x]. Plots and numerical computations for Section 6.

### Section 7. Simple digraph families
* [`Section 7.1.ipynb`][Sec 7.1]. Acyclic digraphs
* [`Section 7.2.ipynb`][Sec 7.2]. Elementary digraphs and complex components
* [`Section 7.x.ipynb`][Sec 7.x]. Plots for Section 7

### Section 8. Numerical results
* [`Section 8.1.ipynb`][Sec 8.1]. Airy integrals
* [`Section 8.2.ipynb`][Sec 8.2]. Testing within the critical window
* [`Section 8.3.ipynb`][Sec 8.3]. Testing outside the critical window

[Sec 1]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%201.ipynb
[Sec 3]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%203.ipynb

[Sec 4.2]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%204.2.ipynb
[Sec 4.3]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%204.3.ipynb

[Sec 5.1]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%205.1.ipynb
[Sec 5.2]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%205.2.ipynb

[Sec 6.1]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%206.1.ipynb
[Sec 6.2]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%206.2.ipynb
[Sec 6.x]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%206.x.ipynb

[Sec 7.1]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%207.1.ipynb
[Sec 7.2]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%207.2.ipynb
[Sec 7.x]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%207.x.ipynb

[Sec 8.1]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%208.1.ipynb
[Sec 8.2]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%208.2.ipynb
[Sec 8.3]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/strong-components-aux/-/raw/master/ipynb/Section%208.3.ipynb

No computations are necessary for Section 2, because there is only an introduction of various generating function types.

## Utility libraries

Along with the `IPython` notebooks, there are several `Python` modules intended for counting of digraph families, exhaustive enumeration, and symbolic computations. They are all containted in `lib` directory.

* `Airy.py`: a small module containing the generalized Airy function, and the complex integration routine for the numerical computation of the Airy integrals.
* `gf_families.py`: a module containing all the generating functions discussed in the paper: DAGs, elementary digraphs, digraphs with one bicyclic component, strongly connected digraphs. All the generating functions are provided in three options: multidigraphs, simple digraphs with or without 2-cycles.
* `exhaustive.py`: a module for exhaustive generation and counting of digraph families.
* `sp_utils.py`: a complementary module for `sympy` adding symbolic expressions for rooted and unrooted trees, Airy functions and its symbolic roots
* `rs_utils.py`: a complementary module for `sympy.ring_series` adding multivariate coefficient extraction and Fourier transform used in the paper
* `nx_utils.py`: a complementary module for `NetworkX` for classifying digraphs according to the excesses of their components
* `arb_utils.py`: a complementary module for `arb`/`FLINT` for fast computing of the probabilities of various digraph families and numerical versions of auxiliary functions for asymptotic enumeration results

## Automated testing

In order to launch all the tests related to utility libraries,
you need to clone the repository and launch
```
pytest
```
It is also possible to test separate modules or run isolated tests (because the full coverage may take a long time). Examples:
```
pytest tests/test_exhaustive.py
pytest tests/test_exhaustive.py -k test_small_examples
```

## Feedback

Contact information is available on my personal webpage
<https://electric-tric.github.io>
