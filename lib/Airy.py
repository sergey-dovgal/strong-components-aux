import scipy.integrate as integrate
import numpy as np
import math

def complex_quad(fun, left, right):
    """
    Complex integration routine.
    Assume that `left` and `right` are complex numbers,
    and we assume a linear trajectory `s = alpha left + (1 - alpha) right`.
    `fun` is a complex-argument function which returns a complex value.
    Invokes `scipy.integrate.quad` with real-valued integration as a subroutine.
    
    N.B. Only finite limits are supported.
    """
    fun_re = lambda z: fun(z).real
    fun_im = lambda z: fun(z).imag
    int_re = integrate.quad(
        lambda t: fun_re(
            t * (right - left) + left
        ),
        0, 1
    )
    int_im = integrate.quad(
        lambda t: fun_im(
            t * (right - left) + left
        ),
        0, 1
    )
    int_cmplx = complex(int_re[0], int_im[0])
    result = int_cmplx * (right - left)
    abserr = abs(complex(int_re[1], int_im[1]) * (right - left))
    return (result, abserr)


def I1(r, x, rlimit = 40):
    """
    For simplicity, we take the integration angle pi/4.
    Later we can update to pi/3 have better convergence.
    """
    return complex_quad(
        lambda t: t**r * np.exp(
            -x * t + t**3/3
        ),
        rlimit * (1 - 1j), 1 - 1j
    )

def I2(r, x, rlimit = 40):
    return complex_quad(
        lambda t: t**r * np.exp(
            -x * t + t**3/3
        ),
        1 - 1j, 1 + 1j
    )

def I3(r, x, rlimit = 40):
    return complex_quad(
        lambda t: t**r * np.exp(
            -x * t + t**3/3
        ),
        1 + 1j, rlimit * (1 + 1j)
    )

def AiryAi(r, x, rlimit = 40):
    """
    Generalised Airy function. The third parameter `rlimit` (optional) stands for the limits of integration.
    Returns a tuple whose first element is the value, and the second is the absolute error of integration.
    """
    i1 = I1(r, x, rlimit)
    i2 = I2(r, x, rlimit)
    i3 = I3(r, x, rlimit)
    integral_result = i1[0] + i2[0] + i3[0]
    abserr = i1[1] + i2[1] + i3[1]
    return (
        (-1)**r / (2 * math.pi * 1j) * integral_result
        ,
        abserr
    )

def I(k, mu):
    return (
        (-1)**k
        / (2 * np.pi * 1j)   # from Cauchy integral
        *
        complex_quad(
            lambda t:
            AiryAi(-k, -t)[0]
            /
            (AiryAi(1, -t)[0])**2
            * np.exp(- mu * t / 2**(1/3) - mu**3 / 6)
            ,
            -20j, 20j
        )[0]
    )