import flint
from flint import arb

exp = flint.arb_series.exp
log = flint.arb_series.log
fpow = lambda x, r: exp(arb(r) * log(x))


####################### MULTIDIGRAPH FLINT POWER SERIES ##################

def QQ(x, y):
    """Rational numbers represented by floating point arbitrary precision numbers
    """
    return arb(x) / arb(y)


def arb_series_hadamard(p, q):
    """Hadamard product
    """
    return flint.arb_series([
        p_i * q_i * arb(flint.fmpz.fac_ui(n))
        for p_i, q_i, n
        in zip(p, q, range(flint.ctx.cap))
    ])


def arb_generalised_exp(r, w):
    """
    Implementation of the generalised deformed exponent from the paper.
    Implements the function $phi_r(z, w)$, also represented as
    `(1 - zw) e^z @ Set(z, w)`
    """
    z = flint.arb_series([0, 1])
    base = fpow(1 - z * w, r) * exp(-z)
    fset = flint.arb_series([
        arb.exp(-n**2 * w / 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    return arb_series_hadamard(base, fset)


def p_dag(n, p):
    """
    Compute the probability that a random multidigraph MD(n,p) is acyclic.
    """
    flint.ctx.cap = n+1
    gf_dag = 1 / arb_generalised_exp(arb(0), p)
    return (
        gf_dag[n]
        * arb(flint.fmpz.fac_ui(n))
        * arb.exp(-arb(n)**2 * arb(p) / 2)
    )


def p_elem(n, p):
    """
    Compute the probability that a random multidigraph MD(n,p) is elementary.
    """
    flint.ctx.cap = n+1
    gf_elem = 1 / arb_generalised_exp(arb(1), p)
    return (
        gf_elem[n]
        * arb(flint.fmpz.fac_ui(n))
        * arb.exp(-arb(n)**2 * arb(p) / 2)
    )


def p_1_pure_bicyclic(n, p):
    """
    Compute the probability that a random multidigraph MD(n,p)
    has one bicyclic strongly connected component with deficiency zero.
    """
    flint.ctx.cap = n+1
    z = flint.arb_series([0, 1])
    base = fpow(1 - z * p, -2) * exp(-z) * p**3 * z**2 / 2
    fset = flint.arb_series([
        arb.exp(-n**2 * p / 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    num = arb_series_hadamard(base, fset)
    gf_1_bi = num / arb_generalised_exp(arb(1), p)**2
    return (
        gf_1_bi[n]
        * arb(flint.fmpz.fac_ui(n))
        * arb.exp(-arb(n)**2 * arb(p) / 2)
    )


def p_1_bicyclic(n, p):
    """
    Compute the probability that a random multidigraph MD(n,p)
    has one bicyclic strongly connected component.
    """
    flint.ctx.cap = n+1
    z = flint.arb_series([0, 1])
    base1 = fpow(1 - z * p, -2) * exp(-z) * p**3 * z**2 / 2
    base2 = fpow(1 - z * p, -1) * exp(-z) * p**2 * z / 2
    fset = flint.arb_series([
        arb.exp(-n**2 * p / 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    num = arb_series_hadamard(base1 + base2, fset)
    gf_1_bi = num / arb_generalised_exp(arb(1), p)**2
    return (
        gf_1_bi[n]
        * arb(flint.fmpz.fac_ui(n))
        * arb.exp(-arb(n)**2 * arb(p) / 2)
    )




####################### SIMPLE DIGRAPH FAMILIES ##################

def p_dag_D(n, p):
    """
    Compute the probability that a random digraph D(n,p) is elementary
    """
    flint.ctx.cap = n+1
    z = flint.arb_series([0, 1])

    w = p / (1 - p)

    fset = flint.arb_series([
        (1 + w) ** (-n * (n-1) // 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    gf_dag = 1 / fset
    return (
        gf_dag[n]
        * arb(flint.fmpz.fac_ui(n))
        * (1 - p) ** (n * (n-1) // 2)
    )


def p_dag_SD(n, p):
    """
    Compute the probability that a random digraph D(n,p) is elementary
    """
    flint.ctx.cap = n+1
    z = flint.arb_series([0, 1])

    w = p / (1 - 2*p)

    fset = flint.arb_series([
        (1 + w) ** (-n * (n-1) // 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    gf_dag = 1 / fset
    return (
        gf_dag[n]
        * arb(flint.fmpz.fac_ui(n))
        * (1 - p) ** (n * (n-1) // 2)
    )


def p_elem_D(n, p):
    """
    Compute the probability that a random digraph D(n,p) is elementary
    """
    flint.ctx.cap = n+1
    z = flint.arb_series([0, 1])

    C_k = z
    w = p / (1 - p)

    base = (1 - z * w) * exp(-z + C_k(z*w))
    fset = flint.arb_series([
        (1 + w) ** (-n * (n-1) // 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    gf_elem = 1 / arb_series_hadamard(base, fset)
    return (
        gf_elem[n]
        * arb(flint.fmpz.fac_ui(n))
        * (1 - p) ** (n * (n-1) // 2)
    )


def p_elem_SD(n, p):
    """
    Compute the probability that a random digraph SD(n,p) is elementary
    """
    flint.ctx.cap = n+1
    z = flint.arb_series([0, 1])

    C_k = z + z**2/2
    w = p / (1 - 2 * p)

    base = (1 - z * w) * exp(-z + C_k(z*w))
    fset = flint.arb_series([
        (1 + w) ** (-n * (n-1) // 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    gf_elem = 1 / arb_series_hadamard(base, fset)
    return (
        gf_elem[n]
        * arb(flint.fmpz.fac_ui(n))
        * (1 - p) ** (n * (n-1) // 2)
    )



def p_1_bicyclic_D(n, p):
    """
    Compute the probability that a random digraph D(n,p)
    has one bicyclic strongly connected component.
    """
    flint.ctx.cap = n+1
    z = flint.arb_series([0, 1])


    C_k = z
    w = p / (1 - p)

    base0 = (1 - z * w) * exp(-z + C_k(z*w))
    base1 = fpow(1 - z * w, -2) * exp(-z + C_k(z*w)) * w**4 * z**3 * (2 - w*z) / 2
    base2 = fpow(1 - z * w, -1) * exp(-z + C_k(z*w)) * w**4 * z**3 / 2
    fset = flint.arb_series([
        (1 + w) ** (-n * (n-1) // 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    num = arb_series_hadamard(base1 + base2, fset)
    den = arb_series_hadamard(base0, fset) ** 2
    gf_1_bi = num / den
    return (
        gf_1_bi[n]
        * arb(flint.fmpz.fac_ui(n))
        * (1 - p) ** (n * (n-1) // 2)
    )


def p_1_bicyclic_SD(n, p):
    """
    Compute the probability that a random digraph SD(n,p)
    has one bicyclic strongly connected component.
    """
    flint.ctx.cap = n+1
    z = flint.arb_series([0, 1])


    C_k = z + z**2/2
    w = p / (1 - 2*p)

    base0 = (1 - z * w) * exp(-z + C_k(z*w))
    base1 = fpow(1 - z * w, -2) * exp(-z + C_k(z*w)) * w**5 * z**4 * (3 - 2*w*z) / 2
    base2 = fpow(1 - z * w, -1) * exp(-z + C_k(z*w)) * w**6 * z**5 / 2
    fset = flint.arb_series([
        (1 + w) ** (-n * (n-1) // 2)
        / arb(flint.fmpz.fac_ui(n))
        for n in range(flint.ctx.cap)
    ])
    num = arb_series_hadamard(base1 + base2, fset)
    den = arb_series_hadamard(base0, fset) ** 2
    gf_1_bi = num / den
    return (
        gf_1_bi[n]
        * arb(flint.fmpz.fac_ui(n))
        * (1 - p) ** (n * (n-1) // 2)
    )

############## THEORETICAL PREDICTIONS ######################


from scipy import special
ai_zero = special.ai_zeros(1)[0][0]
ai_prime_of_aj = special.ai_zeros(1)[3][0]
ai_prime_zero = special.ai_zeros(1)[1][0]
ai_of_aj_prime = special.ai_zeros(1)[2][0]


def alpha(_lam):
    """
    This function implements `alpha(lambda)` from the paper.
    """
    lam = arb(_lam)
    return (lam ** 2 - 1) / (2 * lam) - arb.log(lam)


def beta(_lam):
    """
    This function implements `beta(lambda)` from the paper.
    """
    lam = arb(_lam)
    return (2*lam)**QQ(-1, 3) * (lam - 1)


def gamma2(_lam):
    """
    This function implements `gamma_2(lambda)` from the paper.
    """
    lam = arb(_lam)
    return (
        arb(2) ** QQ(-2, 3)
        / ai_prime_of_aj
        * lam ** QQ(5, 6)
        * arb.exp((lam-1)/6)
    )


def sigma2(_lam):
    """
    This function implements `sigma_2(lambda)` from the paper.
    """
    lam = arb(_lam)
    return - (
        lam**QQ(1, 2)
        * arb.exp(-(lam-1)/6)
        / (
            2 * ai_prime_zero * ai_of_aj_prime
        )
    )


from Airy import AiryAi

def sigma_r(_lam, r):
    """
    This function implements `sigma_{r,d}(lambda)` from the paper,
    in the case where the deficiency `d` is zero.
    """
    lam = arb(_lam)

    return (-1) ** (3*r + 1) * (
        2 ** (QQ(-4, 3) - r)
        * (lam - 1)
        * lam**QQ(1, 6)
        * arb.exp(QQ(1,6) - lam/6)
        * AiryAi(1 - 3*r, ai_prime_zero)[0].real
        / (
            ai_prime_zero * ai_of_aj_prime
        )**2
    )


def omega_elem(_lam):
    """
    This function implements `omega_{elem}(lambda)` from the paper
    """
    lam = arb(_lam)

    return - (
        lam ** QQ(1,2)
        / (
            2 * ai_prime_zero * ai_of_aj_prime
        )
        * arb.exp(-lam**2 / 4 + lam / 3 - QQ(1, 12))
    )


def omega_r(_lam, r):
    """
    This function implements `sigma_{r,d}(lambda)` from the paper,
    in the case where the deficiency `d` is zero.
    """
    lam = arb(_lam)

    return (-1) ** (3*r + 1) * (
        2 ** (QQ(-4, 3) - r)
        * (lam - 1)
        * lam**QQ(1, 6)
        * arb.exp(-lam**2/4 + lam/3 - QQ(1,12))
        * AiryAi(1 - 3*r, ai_prime_zero)[0].real
        / (
            ai_prime_zero * ai_of_aj_prime
        )**2
    )
