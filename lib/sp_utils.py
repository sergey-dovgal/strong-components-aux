import sympy as sp

z = sp.Symbol('z')


###############################################
##### EGFs of ROOTED and UNROOTED TREES
###############################################

class T(sp.Function):
    """Generating function of rooted trees
    """
    def fdiff(self, argindex=1):
        if argindex == 1:
            z = self.args[0]
            return T(z) / z / (1 - T(z))
        else:
            raise ArgumentIndexError(self, argindex)
    def _eval_as_leading_term(self, x, cdir=0):
        """We are only computing Taylor series around the singularity in this project.
        Therefore, the leading term of T is always equal to 1.
        """
        return 1


def U(z):
    """Generating function of unrooted trees
    """
    return T(z) - T(z)**2/2


def simplify(expr):
    """We create an automatic operation that simplifies the expression
    according to the definition of T. We are using the **wildcards** to replace
    $e^{T(z)}$ with $T(z)/z$.
    """
    a = sp.Wild('a')
    return (
        sp
        .expand_power_exp(expr.expand())
        .replace(sp.exp(T(a)), T(a)/a)
    ).simplify()


#################################
##### SYMBOLIC Airy FUNCTION
#################################

# Symbolic roots of the Airy function are `aj` and `ajprime`

aj = sp.Symbol('a_j')           # theoretically, the best possible implementation
                                # is using a class that defines numerical values.
                                # but we don't need this functionality in the project.
ajprime = sp.Symbol("{a'_j}")   # braces required for correct latex rendering


class Ai(sp.airyai):
    """Symbolic Airy function. Inherits all the properties of the usual Airy function.
    """
    def fdiff(self, argindex=1):
        if argindex == 1:
            z = self.args[0]
            return Aiprime(z)
        else:
            raise ArgumentIndexError(self, argindex)

    @classmethod
    def eval(cls, arg):
        """Returns None if the expression should not be modified
        """
        if arg == aj:
            return sp.S.Zero
        
        
class Aiprime(sp.airyaiprime):
    """Symbolic derivative of the Airy function. Inherits all the properties of the usual
    derivative of the Airy function.
    """
    def fdiff(self, argindex=1):
        if argindex == 1:
            z = self.args[0]
            return z * Ai(z)
        else:
            raise ArgumentIndexError(self, argindex)
            
    @classmethod
    def eval(cls, arg):
        if arg == ajprime:
            return sp.S.Zero