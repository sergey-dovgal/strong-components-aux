import networkx as nx            # graph algorithms and data types
import itertools                 # useful for exhaustive generation
from itertools import chain      # concatenations of Python lists and ranges

import nx_utils                  # custom operations on graphs
from nx_utils import get_MDigraph_weight
from nx_utils import is_elementary, is_strict
from nx_utils import number_of_bicycles

def count_DiFamily(n, m, P, multi=False, d2=False):
    """Outputs how many directed graphs with `n` nodes and `m` edges
    satisfy a property P.
    A Boolean flag `multi` specifies whether
    the search is among simple digraphs or multidigraphs.
    `d2` is True if 2-cycles are allowed in the digraph.
    If `multi` is `True`, then `d2` is ignored.
    """
    vertices = range(n)
    if multi:
        edges = [
            (p, q)
            for p in range(n)
            for q in range(n)
        ]
        iterset = itertools.combinations_with_replacement(edges, m)
    else:
        edges = [
            (p, q)
            for p in range(n)
            for q in chain(range(p), range(p + 1, n)) # skip p
        ]
        iterset = itertools.combinations(edges, m)

    total_count = 0

    for E in iterset:
        if multi:
            G = nx.MultiDiGraph()
        else:
            G = nx.DiGraph()
        G.add_nodes_from(range(n))

        for e in E:
            G.add_edge(e[0], e[1])

        if not multi:
            if d2 == False and is_strict(G) == False:
                continue
            
        if P(G):
            if multi:
                total_count += nx_utils.get_MDigraph_weight(G)
            else:
                total_count += 1
    return total_count


def count_DAGs(n, m, **kwargs):
    """Outputs how many DAGs with `n` nodes and `m` edges.
    A Boolean flag `multi` specifies the model (simple digraphs
    or multidigraphs).
    """
    return count_DiFamily(n, m, P=nx.is_directed_acyclic_graph, **kwargs)


def count_elem(n, m, **kwargs):
    """Outputs how many elementary simple digraphs there are with `n` vertices
    and `m` edges. A Boolean flag `multi` specifies the model (simple digraphs
    or multidigraphs).
    """
    return count_DiFamily(n, m, P=nx_utils.is_elementary,  **kwargs)


def count_scc(n, m, **kwargs):
    """Outputs how many strongly connected digraphs there are with `n` vertices
    and `m` edges.
    """
    return count_DiFamily(n, m, P=nx.is_strongly_connected,  **kwargs)


def count_with_one_bicycle(n, m, **kwargs):
    """Outputs how many digraphs have exactly one strongly connected bicyclic component.
    """
    return count_DiFamily(n, m, P=lambda G: number_of_bicycles(G) == 1, **kwargs)