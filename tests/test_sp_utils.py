import sympy as sp

x = sp.Symbol('x')
z = sp.Symbol('z')

import sys
import os.path as osp
sys.path.append(osp.join(osp.dirname(__file__), '..', 'lib'))

from sp_utils import T, U, simplify
from sp_utils import Ai, Aiprime, aj, ajprime

####################################
#### COMBINATORIAL PROPERTIES OF T
####################################

def test_T():
    ### First property: T(z) = z * e^{T(z)}
    expr = z * sp.exp(T(z))
    assert T(z) == simplify(z * sp.exp(T(z)))

    ### Second property: T'(z) = T(z) / ( z (1 - T(z)) )
    ### We are using implicit differentiation to check it
    assert simplify(
        sp.idiff(x - z * sp.exp(x), x, z)
        .subs(x, T(z))
        - T(z).diff(z)
    ) == 0

    assert simplify(T(z).diff(z) - T(z) / z / (1 - T(z))) == 0


####################################
#### COMBINATORIAL PROPERTIES OF U
####################################

def test_U():
    ### Pointed derivative of U is T
    assert simplify(z * U(z).diff(z) - T(z)) == 0

####################################
#### Airy FUNCTION
####################################

def test_Ai():
    assert sp.diff(Ai(z)) == Aiprime(z)
    assert sp.diff(Aiprime(z)) == z * Ai(z)
    assert Ai(aj) == 0
    assert Aiprime(ajprime) == 0

    ## The recursive property of the Airy function is
    ## Ai(k+3; z) = (k+1) Ai(k; z) + z * Ai(k+1; z)
    for k in range(10):
        assert simplify(
            Ai(z).diff(z, k+3)
            - (k+1) * Ai(z).diff(z, k)
            - z * Ai(z).diff(z, k+1)
        ) == 0
