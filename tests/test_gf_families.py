import sympy as sp
from sympy.polys import ring, QQ
from sympy.polys.ring_series import rs_mul, rs_exp, rs_pow
from sympy.polys.ring_series import rs_series_inversion

import sys
import os.path as osp
sys.path.append(osp.join(osp.dirname(__file__), '..', 'lib'))

from gf_families import gf_dags, gf_mdags
from gf_families import gf_elem_multi
from gf_families import count_DAGs

from rs_utils import rs_generalised_exp


####################################
#### MANUAL TESTS, SMALL EXAMPLES
####################################

def test_small_examples():
    # When n=0, there is a unique DAG with no vertices
    assert count_DAGs(0,0) == 1
    assert count_DAGs(0,1) == 0
    assert count_DAGs(0,2) == 0
    # When n=1, there is a unique DAG with one vertex
    assert count_DAGs(1,0) == 1
    assert count_DAGs(1,1) == 0
    assert count_DAGs(1,2) == 0
    # When n=2, and m=0, there is a unique DAG on isolated vertices
    assert count_DAGs(2,0) == 1
    # When n=2, and m=1, there are two labelled DAGs
    assert count_DAGs(2,1) == 2
    assert count_DAGs(2,2) == 0
    assert count_DAGs(2,3) == 0
    # (n,m) = (3, 1) there are 6 possibilities to put one directed edge
    assert count_DAGs(3,1) == 6
    # (n,m) = (3, 2) two edges never form a cycle except they link
    # identical vertices. 6 possibilities for the first edge.
    # The second edge should not be opposite or identical, leaving 4 possibilities.
    # The order of the edges is not fixed, so division by 2 is necessary.
    assert count_DAGs(3,2) == 12
    # (n,m) = (3, 3) the only possibility is a triangle,
    # but the orientations are chosen in such a way that no cycles appear.
    # Among 8 orientations 2 contain cycles, leaving only 6 options.
    assert count_DAGs(3,3) == 6
    # (n,m) = (4,3)
    #      three outgoing arrows to a vertex ($4\cdot 8 = 32$ possibilities)
    #      a path of three edges with any orientations ($4! \cdot 2^3 = 96$)
    #      triangle and an isolated vertex (24)
    #      Total: 24 + 96 + 32 = 152
    assert count_DAGs(4,3) == 152


#######################################
#### TEST SELECTED GFs FOR INVERSION
#######################################

def check_inverse(z, w, n, m, gf, expected_inverse):
    gf_inverse = lambda z,w,n,m: rs_mul(
        rs_series_inversion(gf(z,w,n,m), z, n),
        1 + 0*w, w, m)
    print(n,m)
    print(gf_inverse(z,w,n,m))
    print(expected_inverse(z,w,n,m))
    return gf_inverse(z,w,n,m) == expected_inverse(z,w,n,m)


def test_all_gfs():
    MAX_MN = 10
    R, z, w = ring('z,w', QQ)
    for n in range(1, MAX_MN):
        for m in range(1, MAX_MN):
            gf_dags_inverse = lambda z,w,n,m: sum([
                (-z)**n
                / sp.factorial(n)
                * rs_pow(1 + w, -n*(n-1) // 2, w, m)
                for n in range(n)
            ])
            assert check_inverse(z,w,n,m, gf_dags, gf_dags_inverse)

            gf_mdags_inverse = lambda z,w,n,m: sum([
                (-z)**n
                / sp.factorial(n)
                * rs_exp(-w * n**2 / 2, w, m)
                for n in range(n)
            ])
            assert check_inverse(z,w,n,m, gf_mdags, gf_mdags_inverse)

            assert check_inverse(z,w,n,m,
                gf_elem_multi,
                lambda z,w,n,m: rs_generalised_exp(1, z, w, n, m))


